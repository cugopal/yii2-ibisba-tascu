<?php

namespace app\models;

use app\components\MyMailer;
use dektrium\user\models\User as BaseUser;
use dektrium\user\models\Token;

class User extends BaseUser
{
    protected function getMailer()
    {
        return \Yii::$container->get(MyMailer::className());
    }

  /* override attemptConfirmation to send an email to the User
   * once admin confirms the account creation
  */

  /**
   * Attempts user confirmation.
   *
   * @param string $code Confirmation code.
   *
   * @return boolean
   */
  public function attemptConfirmation($code)
  {
      $token = $this->finder->findTokenByParams($this->id, $code, Token::TYPE_CONFIRMATION);

      if ($token instanceof Token && !$token->isExpired) {
          $token->delete();
          if (($success = $this->confirm())) {
              //sendConfirmationMessage to the user with token=>null, showPassword=>false
              $this->mailer->sendConfirmationMessage($this, null, false);
              $message = \Yii::t('user', 'Thank you, registration is now complete.');
              \Yii::$app->user->login($this, $this->module->rememberFor);
          } else {
              $message = \Yii::t('user', 'Something went wrong and your account has not been confirmed.');
          }
      } else {
          $success = false;
          $message = \Yii::t('user', 'The confirmation link is invalid or expired. Please try requesting a new one.');
      }

      \Yii::$app->session->setFlash($success ? 'success' : 'danger', $message);

      return $success;
  }


}
/*
class User extends \yii\base\BaseObject implements \yii\web\IdentityInterface
{
    public $id;
    public $username;
    public $password;
    public $authKey;
    public $accessToken;

    private static $users = [
        '100' => [
            'id' => '100',
            'username' => 'admin',
            'password' => 'admin',
            'authKey' => 'test100key',
            'accessToken' => '100-token',
        ],
        '101' => [
            'id' => '101',
            'username' => 'demo',
            'password' => 'demo',
            'authKey' => 'test101key',
            'accessToken' => '101-token',
        ],
    ];

    */

    /**
     * {@inheritdoc}
     */
     /*
    public static function findIdentity($id)
    {
        return isset(self::$users[$id]) ? new static(self::$users[$id]) : null;
    }*/

    /**
     * {@inheritdoc}
     */
     /*
    public static function findIdentityByAccessToken($token, $type = null)
    {
        foreach (self::$users as $user) {
            if ($user['accessToken'] === $token) {
                return new static($user);
            }
        }

        return null;
    }
    */

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
     /*
    public static function findByUsername($username)
    {
        foreach (self::$users as $user) {
            if (strcasecmp($user['username'], $username) === 0) {
                return new static($user);
            }
        }

        return null;
    }
    */

    /**
     * {@inheritdoc}
     */
     /*
    public function getId()
    {
        return $this->id;
    }*/

    /**
     * {@inheritdoc}
     */
     /*
    public function getAuthKey()
    {
        return $this->authKey;
    }*/

    /**
     * {@inheritdoc}
     */
     /*
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    */
    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
     /*
    public function validatePassword($password)
    {
        return $this->password === $password;
    }
} */
