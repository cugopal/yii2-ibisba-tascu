# How to install



1. Install the Yii2 basic template (`yii2-app-basic`) using `composer` (see https://github.com/yiisoft/yii2-app-basic)

```
composer create-project --prefer-dist yiisoft/yii2-app-basic tascu_app
```

2. Check out yii2-ibisba-tascu (outside the tascu_app hierarchy).

3. Copy the model, view, and controller files to tascu_app