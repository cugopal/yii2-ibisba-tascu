<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'tascu',
		'name' => 'TasCu',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'zXQUiuwmaxxk5CGlg8XrdtHFSGouuNBD',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            //'identityClass' => 'dektrium\user\models\User',
            //'enableAutoLogin' => false,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
          'class' => 'yii\swiftmailer\Mailer',
          'viewPath' => '@app/mail',
          // send all mails to a file by default. You have to set
          // 'useFileTransport' to false and configure a transport
          // for the mailer to send real emails.
          'useFileTransport' => false,
          'transport' => [
            'class' => 'Swift_SmtpTransport',
            'host' => 'smtp.gmail.com',
            'username' => 'tascu.system',
            'password' => 'mxkevzogorcvmnuq',
            'port' => 465,
            'encryption' => 'ssl',
          ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'view' => [
          'theme' => [
            'pathMap' => [
              '@dektrium/user/views' => '@app/views/user'
            ],
          ],
        ],
        'db' => $db,
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => true,
/*
            'rules' => [
            ], */
        ],
    ],
    'as beforeRequest' => [
    'class' => 'yii\filters\AccessControl',
    'rules' => [
        [
            'allow' => true,
            // Warning: the following lines opens the named actions in ANY controller.
            // For example, if you include 'index' in allowed actions, it will open up
            // site/index as well as any other controller/index, unless you explicitly
            // redirect the guest to /user/security/login in your controller/index action
            'actions' => ['login', 'error', 'forgot', 'request', 'reset', 'register', 'resend'],
        ],
        [
            'allow' => true,
            'roles' => ['@'],
        ],
    ],
    'denyCallback' => function () {
        return Yii::$app->response->redirect(['user/security/login']);
    },
],
    'modules' => [
/*
      'attachments' => [
        'class' => nemmo\attachments\Module::className(),
        'tempPath' => '@app/uploads/temp',
        'storePath' => '@app/uploads/store',
        'rules' => [ // Rules according to the FileValidator
          'maxFiles' => 10, // Allow to upload maximum 3 files, default to 3
          //'mimeTypes' => 'image/png', // Only png images
          'maxSize' => 1024 * 1024 * 5,// 1 MB
        ],
        'tableName' => '{{%attachments}}', // Optional, default to 'attach_file'
      ], */
      'gridview' => [
        'class' => '\kartik\grid\Module',
      ],
      'treemanager' =>  [
        'class' => '\kartik\tree\Module',
        // other module settings, refer detailed documentation
      ],
      'user' => [
        'class' => 'dektrium\user\Module',
        'modelMap' => [
          'User' => 'app\models\User',
          'RegistrationForm' => 'app\models\RegistrationForm',
        ],
        'mailer' => [
          //'class' => 'app\components\MyMailer',
          'sender' => ['tascu.system@gmail.com' => 'TasCu'],
        ],
        'admins' => ['gopal.peddinti@vtt.fi','peter.blomberg@vtt.fi'],
        'rememberFor' => 60,
        'enableConfirmation' => true,
        'enableFlashMessages' => false,
        //'enableActivationByAdminIsRequired' => true,
      ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

/*
Yii::$container->set('leandrogehlen\treegrid\TreeGridAsset',[
    'js' => [
        'js/jquery.cookie.js',
        'js/jquery.treegrid.min.js',
    ]
]); */
return $config;
