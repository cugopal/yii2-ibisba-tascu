<?php
namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\data\ActiveDataProvider;
use app\models\Tgstep;
use yii\filters\VerbFilter;
use yii\web\ConflictHttpException;
use yii\web\NotFoundHttpException;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;
use yii\web\Response;
use app\models\Modelhistory;


class TgstepController extends Controller
{

  public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /*
    public function beforeAction($action) {
      if(Yii::$app->user->isGuest) {
        return $this->redirect(Yii::$app->user->loginUrl);
      } else {
        return true;
      }
    } */

    public function actionGetTree () {
      Yii::$app->response->format = Response::FORMAT_JSON;
      $query = Tgstep::find()->where(['is_deleted'=>false])->orderBy([
        'parent_id'=>SORT_ASC, 'position'=>SORT_ASC]);
      if (null === $rows = $query->asArray()->all()) {
        return [];
      }

      $result = [];

      foreach ($rows as $row) {
        $parent = ArrayHelper::getValue($row, 'parent_id', 0);
        $item = [
          'id' => ArrayHelper::getValue($row, 'id', 0),
          'parent' => ($parent) ? $parent : '#',
          'text' => ArrayHelper::getValue($row, 'step_name', 'item'),
          'a_attr' => [
            'data-id' => ArrayHelper::getValue($row, 'id', 0),//$row['id'],
            'data-parent_id' => ArrayHelper::getValue($row, 'parent_id', 0)//$row['parent_id']
          ],
        ];
        $result[$row['id']] = $item;
      }

      Yii::$app->response->format = Response::FORMAT_RAW;
      header('Content-Type: application/json');
      return json_encode(array_values($result));
    }
    /**
     * Lists all Tree models.*
     * @return mixed
     */
    public function actionIndex()
    {
        $id = Yii::$app->request->get('id');
        $initial = Tgstep::findOne($id);
        $query = Tgstep::find()->where(['is_deleted'=>false])->orderBy([
          'parent_id'=>SORT_ASC, 'position'=>SORT_ASC]);
          $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
            'sort' => [
              'defaultOrder' => [
                'parent_id' => SORT_ASC,
                'position' => SORT_ASC,
              ]
            ],
          ]);

          return $this->render('index', [
            'dataProvider' => $dataProvider,
            'initial' => $initial,
            'buttonTemplate' => (Yii::$app->user->identity->isAdmin) ?
              '{view}{update}{zoomin}{add}{move}' : '{view}{update}{zoomin}',
          ]);
    }

/*
    public function actionKaIndex() {
        $tree = new \kasoft\jstree\JsTree([
            'modelName' => 'Tgstep',    // * Namespace of the Model
            'modelPropertyId' => 'id',                       // * primary Key
            'modelPropertyParentId' => 'parent_id',          // * Parent ID for tree items
            'modelPropertyPosition' => 'position',          // *for sorting items
            'modelPropertyName' => 'step_name',                  // * Fieldname to show
            'modelFirstParentId' => NULL,                   // * ID for the Tree to start
            'modelPropertyType' => 'type',                  // Item type (for Icon and jsTree rights)
            'controllerId' => 'ka-index',                      // Controler Actions which should handle everything
            'jstreeDiv' => '#jstree',                       // DIV where the Tree will be displayed
            'jstreeIcons' => false,                         // Show Icons or not
            'jstreePlugins' => ["contextmenu", "dnd"],   // Plugins to be load
            'jstreeType' => [                               // jsTree Type Options
                "#" => [
                    "max_children" => -1,
                    "max_depth" => -1,
                    "valid_children" => -1,
                    "icon" => "glyphicon glyphicon-th-list"
                ],
                "default" => [
                    "icon" => "glyphicon glyphicon-question-sign"
                ],
            ]
        ]);

        return $this->render('ka-index');
    } */

    public function actionView($id)
    {
        return $this->render('view', [
          'model' => $this->findModel($id),
        ]);
    }

    public function actionTreeIndex()
    {
      return $this-> render('treeview');
    }

    /* show history of the model with $id */
    public function actionHistory($id)
    {
      $query = Modelhistory::find()->where([
        'table'=> 'dbtl_steps', 'field_id'=>$id]);

      $dataProvider = new ActiveDataProvider([
        'query' => $query]);

      return $this->render('history', [
        'model_id' => $id,
        'dataProvider' => $dataProvider,
      ]);
    }

    /* show recent changes in the table */
    public function actionChanges()
    {
      $query = Modelhistory::find()->where([
        'table'=> 'dbtl_steps']);

      $dataProvider = new ActiveDataProvider([
        'query' => $query,
        'sort' => [
          'defaultOrder' => [
            'date' => SORT_DESC,
          ]
        ],
      ]);

      return $this->render('changes', [
        'dataProvider' => $dataProvider,
      ]);
    }

    public function actionCreate()
    {
      $model = new Tgstep();

      if ($model->load(Yii::$app->request->post()))
      {
        if($model->parent_id == null)
        {
          $model->parent_id = 0;
        }
        $model->position = 1 + Tgstep::countChildren($model->parent_id);
        $model->save();
        return $this->redirect(['view', 'id' => $model->id]);
      }
      else
      {
        return $this->render('create', [
          'model' => $model,
        ]);
      }
    }

    public function actionZoomin($id)
    {
      $branch_ids = Tgstep::getBranch($id);
      //var_dump($branch_ids);
      $query = Tgstep::find(['is_deleted'=>false])->where([
        'id'=>$branch_ids])->orderBy([
          'parent_id'=>SORT_ASC, 'position'=>SORT_ASC]);
      $model = $this->findModel($id);
      $dataProvider = new ActiveDataProvider([
          'query' => $query,
          'pagination' => false,
          'sort' => [
            'defaultOrder' => [
              'parent_id' => SORT_ASC,
              'position' => SORT_ASC,
            ]
          ],
      ]);

        return $this->render('zoom', [
            'dataProvider' => $dataProvider,
            'root_id' => $model->parent_id,
        ]);
    }

    public function actionZoomout($id)
    {
      $model = $this->findModel($id);
      $branch_ids = Tgstep::getBranch($model->parent_id);
      //var_dump($branch_ids);
      $query = Tgstep::find(['is_deleted'=>false])->where([
        'id'=>$branch_ids])->orderBy([
          'parent_id'=>SORT_ASC, 'position'=>SORT_ASC]);
      $dataProvider = new ActiveDataProvider([
          'query' => $query,
          'pagination' => false,
          'sort' => [
            'defaultOrder' => [
              'parent_id' => SORT_ASC,
              'position' => SORT_ASC,
            ]
          ],
      ]);

      $parent_model = $this->findModel($model->parent_id);
        return $this->render('zoom', [
            'dataProvider' => $dataProvider,
            'root_id' => $parent_model->parent_id,
        ]);
    }

    /* actionMove needs to be finalized. Concurrency issues need to be checked */
    public function actionMove($id) {
      $model = $this->findModel($id);
      $old_parent_id = $model->parent_id;

      try {
        if ($model->load(Yii::$app->request->post())) {

          $transaction = Yii::$app->db->beginTransaction();

          // Determine the new parent id
          if($model->before_or_after == 'in') {
            // If "in", then we know that $move_to_target is the new parent_id
            $new_parent_id = $model->move_to_target;
            $model->position = 1 + Tgstep::countChildren($new_parent_id);
          } else {
            // If "before" or "after", note that $move_to_target is a sibling model
            // Therefore, new parent id would be $move_to_target->parent_id
            $sibling_model = $this->findModel($model->move_to_target);
            $new_parent_id = $sibling_model->parent_id;

            // Note: getChildren sorts children in the order of position
            $all_siblings_ids = Tgstep::getChildren($new_parent_id);
            // what if $model->id is among $all_siblings_ids?
            $all_siblings_ids = array_diff($all_siblings_ids, [$model->id=>$model->id]);

            // Note the position where $model should to be inserted.
            // Here we are just noting the position, The insertion comes a few lines
            // later.
              $insert_at = $sibling_model->position;

            // In the scenario that you want to move the $model's position
            // under the same parent (i.e. without changing it to a new
            // it's parent), you need to make and adjustment to the $insert_at
            // if $model->position is before $sibling_model->position.
            // This is because, when you move $model down from its position,
            // you vacate one spot before $sibling_model->position.

            if(($old_parent_id == $new_parent_id) and (
              $model->position < $sibling_model->position)) {
                $insert_at = $insert_at - 1;
            }

            // The actual moving takes place here. To make sure that
            // the positions are indexed from 1 to countChildren($new_parent_id)
            // you need $this_child_pos.
            ///
            $this_child_pos = 0;
            foreach ($all_siblings_ids as $a_sibling_id) {
              $a_sibling_model = $this->findModel($a_sibling_id);
              // you only need to modify the positions starting from $insert_at
              ///
              if($a_sibling_model->position >= $insert_at) {
                if(($model->before_or_after == 'before') and
                ($a_sibling_model->position == $insert_at)) {
                  // to insert before, $model->position should be set
                  // before you set $a_sibling_model->position
                  ///
                  $model->position = ++$this_child_pos;
                }
                $a_sibling_model->position = ++$this_child_pos;
                $a_sibling_model->save();
                if(($model->before_or_after == 'after') and
                ($a_sibling_model->position == $insert_at)) {
                  // to insert after, setting the $model->position should
                  // follow setting $a_sibling_model->position
                  ///
                  $model->position = ++$this_child_pos;
                }
              } else {
                ++$this_child_pos;
              }
            }
          }

          // Finally, set the new parent id for the model
          // This should be done only after the above if-else block
          // because, otherwise Tgstep::getChildren would also include
          // also the $model. Note however, that we have taken care of this
          // eventuality by removing $model->id from $all_siblings_ids, so this
          // should not really be a problem, but even so, setting the parent_id
          // here is better because otherwise it has to be repeated in both if
          // and else blocks.
          $model->parent_id = $new_parent_id;
          $model->save();

          // If model moved to a new parent (i.e. if the new parent is different
          // from old parent, note that the old parent lost a child. So, we
          // need to reindex the children of $old_parent_id so that their positions
          // range from 1 to Tgstep::countChildren($old_parent_id)
          if($old_parent_id != $new_parent_id) {
            $children_of_old_parent = Tgstep::getChildren($old_parent_id);
            $this_child_pos = 0;
            foreach ($children_of_old_parent as $a_child_id) {
              $a_child_model = $this->findModel($a_child_id);
              $a_child_model->position = ++$this_child_pos;
              $a_child_model->save();
            }
          }

          // To avoid Concurrency issues, commit the transaction if no StaleObjectException
          // occurred.
          ///
          $transaction->commit();

          // Once the $model has been moved, show the part of the task tree
          // surrounding the $model. This can be done using zoomin, to show
          // zoomin into $model->parent_id. However, if $model->id == 0,
          // note that you can't show $model->parent_id, because a model
          // with that id doesn't exist. In such case, show $model->id.
          return $this->redirect(['zoomin',
            'id' => ($new_parent_id == 0) ? $model->id : $model->parent_id]);
        } else {
          return $this->render('move', [
              'model' => $model,
          ]);
        }
      } catch(Exception $e) {
        $transaction->rollBack();
        throw $e;
      } catch(Throwable $e) {
        $transaction->rollBack();
        throw $e;
      }  catch(yii\base\Exception $e) {
        $transaction->rollBack();
        throw $e;
      }
    }

    /*
    public function actionChildren() {
      if (isset($_POST['depdrop_parents'])) {
        $parents = $_POST['depdrop_parents'];
        if ($parents != null) {
            $cat_id = $parents[0];
            $excl_child = $_POST['depdrop_params'][0];
            if(Tgstep::countChildren((int)$cat_id) == 0) {
              echo Json::encode(['output'=>'', 'selected'=>'']);
              return;
            } else {
              $out = Tgstep::find()->select([
                'id as id', 'step_name as name'])->where([
                  'parent_id' => $cat_id])->andFilterWhere([
                    '<>', 'id', $excl_child])->orderBy([
                      'position' => SORT_ASC])->asArray()->all();
              echo Json::encode(['output'=>$out, 'selected'=>'']);
            }
        }
      } else {
        echo Json::encode(['output'=>['id'=>'','name'=>'-'], 'selected'=>'']);
      }
    } */

    public function actionAdd($id)
    {
      $model = new Tgstep();
      $model->loadDefaultValues();
      #$id = Yii::$app->request->get('id');
      $model->parent_id = $id;
      $model->position = 1 + Tgstep::countChildren($id);

      if ($model->load(Yii::$app->request->post()) && $model->save())
      {
        return $this->redirect(['index', 'id' => $model->id]);
      }
      else
      {
        return $this->render('create', [
          'model' => $model,
        ]);
      }
    }

       /**
        * Updates an existing Tree model.
        * If update is successful, the browser will be redirected to the 'view' page.
        * @param integer $id
        * @return mixed
        */
       public function actionUpdate($id)
       {
           $model = $this->findModel($id);

           try {
             if ($model->load(Yii::$app->request->post()) && $model->save())
             {
               return $this->redirect(['view', 'id' => $model->id]);
             }
             else
             {
               return $this->render('update', [
                 'model' => $model,
               ]);
             }
           } catch(yii\db\StaleObjectException $e) {
             throw new ConflictHttpException($e->getMessage().
             ' Another user might have updated it while you were at it.'.
             ' If you want to keep a record of the changes you just tried'.
             ' to make, please use the browser\'s back button and copy/paste'.
             ' your changes to a local file.');
           }
       }

       /**
        * Deletes an existing Tree model.
        * If deletion is successful, the browser will be redirected to the 'index' page.
        * @param integer $id
        * @return mixed
        */
       public function actionDelete($id)
       {
           $this->findModel($id)->softDelete();

           return $this->redirect(['index']);
       }

       /**
        * Finds the Tree model based on its primary key value.
        * If the model is not found, a 404 HTTP exception will be thrown.
        * @param integer $id
        * @return Tree the loaded model
        * @throws NotFoundHttpException if the model cannot be found
        */
       protected function findModel($id)
       {
           if (($model = Tgstep::findOne($id)) !== null)
           {
               return $model;
           }
           else
           {
               throw new NotFoundHttpException('The requested model does not exist. Someone was quicker to delete it before you got here.');
           }
       }


  }
  ?>
