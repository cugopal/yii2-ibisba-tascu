<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\Tgstep;

/* @var $this yii\web\View */

$this->title = 'TasCu: DBTL Step '.$model->id;
$this->params['breadcrumbs'][] = ['label' => 'Index', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->id;
?>
<div class="tgstep-view">

    <h1><?= Html::encode('DBTL step id: '. $model->id) ?></h1>

    <p>
        <?php echo Html::a('Show in Index', ['index', 'id' => $model->id], ['class' => 'btn btn-default']) ?>
        <?php //echo  Html::button('Print', ['class' => 'btn btn-default',
          //'onclick' => 'window.print()']) ?>
        <?php //echo Html::a('View history', ['history', 'id' => $model->b_number], ['class' => 'btn btn-default']) ?>
        <?php echo Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a('View history', ['history', 'id' => $model->id],
            ['class' => 'btn btn-default']); ?>
        <?php //echo Html::a('Copy', ['clone', 'id' => $model->b_number], ['class' => 'btn btn-success']) ?>
    </p>

    <div class="gp-detailview">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'parent_id',
            'position',
            'step_name',
            //'responsible_partner',
            'sop_url',
            'organism',
            'part_of_service',
            'duration_in_days',
            'success_rate',
            'step_desc',
            'comments',
            ],
    ]) ?>
  </div>
</div>
