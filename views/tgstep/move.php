<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Tgstep;
use yii\helpers\Url;
use igor\jstree\JsTreeInput;
use iutbay\yii2jstree\JsTree;

$this->title = 'TasCu: Move Step '.$model->id;
$this->params['breadcrumbs'][] = ['label' => 'Index', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id.' - '.$model->step_name, 'url' => ['view', 'id'=>$model->id]];
$this->params['breadcrumbs'][] = 'move to';
?>

<div class="move-form">
  <?php $form = ActiveForm::begin(); ?>
  <?php

  echo $form->field($model, 'before_or_after')->dropDownList([
    'before' => 'before', 'after' => 'after', 'in'=>'in'],
    [
      'prompt' => [
        'text' => 'Choose one option ...',
        'options' => ['value'=>'none', 'class' => 'prompt'],
      ],
    ])
    ->label('Position relative to target');

  echo $form->field($model, 'move_to_target')->widget(JsTreeInput::className(), [
      'treeDataRoute' => ['get-tree'],
      'plugins' => ['changed'],
      /*
      'themeSettings' => [
        'dots' => false,
      ], */
  ])->label('Target');
  ?>

  <div class="form-group">
    <?= Html::submitButton('Move',
    ['class' => 'btn btn-primary']) ?>
    <?php echo Html::a('Cancel', /* ['zoomin',
      'id'=> ($model->parent_id == 0) ? $model->id : $model->parent_id],*/
      ['index', 'id' => $model->id],
      ['class' => 'btn btn-default']) ?>
  </div>
  <?php ActiveForm::end(); ?>
</div>
