
<?php
use leandrogehlen\treegrid\TreeGrid;
use yii\helpers\Html;
use yii\helpers\Url;
?>

<div class="form-group">
  <?php /* echo Html::a('Create DBTL step', ['create'],
    [
      'class' => 'btn btn-success',
      'style'=>'float:right;'
      //'style'=>'width:100%;margin-top:1em;margin-bottom:1em;'
    ]) */ ?>

  <?php //echo $this->render('_search', ['model' => $searchModel]); ?>
</div>
<?php
echo dkhlystov\widgets\TreeGrid::widget([
  'dataProvider' => $dataProvider,
  'showRoots' => true,
  'rootParentId' => $root_id,

  'columns' => [
    [
      'attribute' => 'step_name',
      'headerOptions' => ['style' => 'width:60%;'],
      ],
			[
        'attribute' => 'step_desc',
        'headerOptions' => ['style' => 'width:10%;'],
      ],
      //'responsible_partner',
    /*  [
        'attribute' => 'comments',
        'headerOptions' => ['style' => 'width:20%;'],
      ], */
    [
      'attribute' => 'sop_url',
      'headerOptions' => ['style' => 'width:10%;'],

    ],
      [
        'class' => 'yii\grid\ActionColumn',
        'headerOptions' => ['style' => 'width:10%;'],
        #'template' => '{view}{update}{zoomin}{zoomout}{add}{move}',
        'template' => (Yii::$app->user->identity->isAdmin) ? '{view}{update}{zoomin}{zoomout}{add}{move}' : '{view}{update}{zoomin}{zoomout}',
        'buttons' => [
          'zoomin' => function ($url) {
            return Html::a(
              '<span class="glyphicon glyphicon-zoom-in"></span>',
              $url,
              [
                'title' => 'zoomin',
                'data-pjax' => '0',
              ]
            );
          },
          'zoomout' => function ($url) {
            return Html::a(
              '<span class="glyphicon glyphicon-zoom-out"></span>',
              $url,
              [
                'title' => 'zoomout',
                'data-pjax' => '0',
              ]
            );
          },
          'add' => function ($url) {
            return Html::a(
              '<span class="glyphicon glyphicon-plus"></span>',
              $url,
              [
                'title' => 'addChild',
                'data-pjax' => '0',
              ]
            );
          },
          'move' => function ($url) {
            return Html::a(
              '<span class="glyphicon glyphicon-move"></span>',
              $url,
              [
                'title' => 'move',
                'data-pjax' => '0',
              ]
            );
          },
        ],

        'urlCreator' => function ($action, $model, $key, $index) {
          if ($action === 'zoomin') {
            $url = Url::toRoute(['tgstep/zoomin', 'id' => $model->id]);
            return $url;
          } elseif ($action === 'zoomout') {
            if($model->parent_id == 0) {
              $url = Url::toRoute(['tgstep/index']);
            } else {
              $url = Url::toRoute(['tgstep/zoomout', 'id' => $model->id]);
            }
            return $url;
          } elseif ($action === 'add') {
            $url = Url::toRoute(['tgstep/add', 'id' => $model->id]);
            return $url;
          } elseif ($action === 'view') {
            $url = Url::toRoute(['tgstep/view', 'id' => $model->id]);
            return $url;
          } elseif ($action === 'update') {
            $url = Url::toRoute(['tgstep/update', 'id' => $model->id]);
            return $url;
          } /* elseif ($action === 'delete') {
            $url = Url::toRoute(['tgstep/delete', 'id' => $model->id]);
            return $url;
          } */ elseif ($action === 'move') {
          $url = Url::toRoute(['tgstep/move', 'id' => $model->id]);
          return $url;
        }
      }
    ],
  ]
]);
/*
echo TreeGrid::widget([
    'dataProvider' => $dataProvider,
    'keyColumnName' => 'id',
    'parentColumnName' => 'parent_id',
    'parentRootValue' => $root_id, //first parentId value
    'pluginOptions' => [
      // For zoom-in and zoom-out to work, the initialState must be 'expanded'
        'initialState' => 'expanded',
        'expanderExpandedClass' => 'glyphicon glyphicon-chevron-down',
        'expanderCollapsedClass' => 'glyphicon glyphicon-chevron-right',
    ],
    'columns' => [
      //'parent_id',
      'step_name',
      'responsible_partner',
      'sop_url',
      //'step_desc',
      'comments',
      [
        'class' => 'yii\grid\ActionColumn',
        'template' => '{view}{update}{delete}{zoomin}{zoomout}{add}',
        'buttons' => [
          'zoomin' => function ($url) {
            return Html::a(
              '<span class="glyphicon glyphicon-zoom-in"></span>',
              $url,
              [
                'title' => 'zoomin',
                'data-pjax' => '0',
              ]
            );
          },
          'zoomout' => function ($url) {
            return Html::a(
              '<span class="glyphicon glyphicon-zoom-out"></span>',
              $url,
              [
                'title' => 'zoomout',
                'data-pjax' => '0',
              ]
            );
          },
          'add' => function ($url) {
            return Html::a(
              '<span class="glyphicon glyphicon-plus"></span>',
              $url,
              [
                'title' => 'addChild',
                'data-pjax' => '0',
              ]
            );
          },
        ],
        'urlCreator' => function ($action, $model, $key, $index) {
          if ($action === 'zoomin') {
            $url = Url::toRoute(['tgstep/zoomin', 'id' => $model->id]);
            return $url;
          } elseif ($action === 'zoomout') {
            if($model->parent_id == 0) {
              $url = Url::toRoute(['tgstep/index']);
            } else {
              $url = Url::toRoute(['tgstep/zoomout', 'id' => $model->id]);
            }
            return $url;
          } elseif ($action === 'add') {
            $url = Url::toRoute(['tgstep/add', 'id' => $model->id]);
            return $url;
          } elseif ($action === 'view') {
            $url = Url::toRoute(['tgstep/view', 'id' => $model->id]);
            return $url;
          } elseif ($action === 'update') {
            $url = Url::toRoute(['tgstep/update', 'id' => $model->id]);
            return $url;
          } elseif ($action === 'delete') {
            $url = Url::toRoute(['tgstep/delete', 'id' => $model->id]);
            return $url;
          }
        }
      ],
    ]
]); */
?>
