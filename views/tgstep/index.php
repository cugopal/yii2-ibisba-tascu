<?php
use leandrogehlen\treegrid\TreeGrid;
use yii\helpers\Html;
use yii\helpers\Url;
use Yii;

$this->title = 'TasCu: DBTL Index';
$this->params['breadcrumbs'][] = ['label' => 'Index', 'url' => ['index']];
?>

<div class="form-group">
  <?php /* echo Html::a('Create DBTL step', ['create'],
    [
      'class' => 'btn btn-success',
      'style'=>'float:right;'
      //'style'=>'width:100%;margin-top:1em;margin-bottom:1em;'
    ]) */ ?>

  <?php //echo $this->render('_search', ['model' => $searchModel]); ?>
</div>
<?php
echo dkhlystov\widgets\TreeGrid::widget([
  'dataProvider' => $dataProvider,
  'initialNode' => $initial,
  'showRoots' => true,

  /* These pluginOptions don't work, but are there begging to be debugged */
  'pluginOptions' => [
    'saveState' => 'true',
    'saveStateMethod' => 'cookie',
    //'style' => 'table-layout:inherit;'
  ],
  'columns' => [
    [
      'attribute' => 'step_name',
      'headerOptions' => ['style' => 'width:60%;'],
    ],
    [
      'attribute' => 'step_desc',
      'headerOptions' => ['style' => 'width:20%;'],
    ],
    //'responsible_partner',
/*
    [
      'attribute' => 'comments',
      'headerOptions' => ['style' => 'width:20%;'],
    ], */
    [
      'attribute' => 'sop_url',
      'headerOptions' => ['style' => 'width:10%;'],

    ],
    [
      'class' => 'yii\grid\ActionColumn',
      'headerOptions' => ['style' => 'width:10%;'],

      'template' => $buttonTemplate,
      #'template' => (Yii::$app->user->identity->isAdmin) ? '{view}{update}{zoomin}{add}{move}' : '{view}{update}{zoomin}',
      #'template' => (Yii::$app->user->can('admin')) ? '{view}{update}{zoomin}{add}{move}' : '{view}{update}{zoomin}',
      'buttons' => [
        'zoomin' => function ($url) {
          return Html::a(
            '<span class="glyphicon glyphicon-zoom-in"></span>',
            $url,
            [
              'title' => 'zoomin',
              'data-pjax' => '0',
            ]
          );
        },
        'add' => function ($url) {
          return Html::a(
            '<span class="glyphicon glyphicon-plus"></span>',
            $url,
            [
              'title' => 'addChild',
              'data-pjax' => '0',
            ]
          );
        },
        'move' => function ($url) {
          return Html::a(
            '<span class="glyphicon glyphicon-move"></span>',
            $url,
            [
              'title' => 'move',
              'data-pjax' => '0',
            ]
          );
        },
      ],
      'urlCreator' => function ($action, $model, $key, $index) {
        if ($action === 'zoomin') {
          $url = Url::toRoute(['tgstep/zoomin', 'id' => $model->id]);
          return $url;
        } elseif ($action === 'add') {
          $url = Url::toRoute(['tgstep/add', 'id' => $model->id]);
          return $url;
        } elseif ($action === 'view') {
          $url = Url::toRoute(['tgstep/view', 'id' => $model->id]);
          return $url;
        } elseif ($action === 'update') {
          $url = Url::toRoute(['tgstep/update', 'id' => $model->id]);
          return $url;
        } elseif ($action === 'delete') {
          $url = Url::toRoute(['tgstep/delete', 'id' => $model->id]);
          return $url;
        } elseif ($action === 'move') {
          $url = Url::toRoute(['tgstep/move', 'id' => $model->id]);
          return $url;
        }
      }
    ],
  ]
]);
?>
