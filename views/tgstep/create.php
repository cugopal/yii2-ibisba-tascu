<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Moclo */

$this->title = 'TasCu: create DBTL step';
$this->params['breadcrumbs'][] = ['label' => 'Index', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Create';
?>
<div class="tgstep-create">

    <h1><?= Html::encode('Create DBTL step') ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
