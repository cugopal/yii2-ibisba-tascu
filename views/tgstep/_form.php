<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Tgstep;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\Plasmids */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tgstep-form">

    <?php $form = ActiveForm::begin();

    if(Yii::$app->user->identity->isAdmin) {
      echo $form->field($model, 'step_name')->textInput(['maxlength' => true,
        'placeholder' => 'Enter the name of DBTL step']);
      echo $form->field($model, 'step_desc')->textarea(['rows' => 6,
        'placehoder' => 'Provide a description']);
      echo $form->field($model, 'sop_url')->textInput(['maxlength' => true,
        'placeholder' => 'URL where the SOP can be accessed from']);

      $mspecies = [
        'Saccharomyces cerevisiae' => 'Saccharomyces cerevisiae',
        'Pseudomonas putida' => 'Pseudomonas putida',
      ];
      echo $form->field($model, 'organism')->widget(Select2::classname(), [
        'model' => $model,
        'data' => $mspecies,
        'theme' => Select2::THEME_DEFAULT,
        'options' => ['placeholder' => 'Type for hints. You can also add a new value and end it with ENTER'],
        'pluginOptions' => [
          'allowClear' => true,
          'tags' => true,
        ],
      ]);
      $services = [
        'Protein production' => 'Protein production',
      ];
      echo $form->field($model, 'part_of_service')->widget(Select2::classname(), [
        'model'=>$model,
        'data' => $services,
        'theme' => Select2::THEME_DEFAULT,
        'options' => ['placeholder' => 'Type for hints. You can also add a new value and end it with ENTER'],
        'pluginOptions' => [
          'allowClear' => true,
          'tags' => true,
        ],
      ]);
      echo $form->field($model, 'duration_in_days')->textInput(['maxlength' => true,
        'placeholder' => 'Time required to execute the step (in days) e.g. 0.5, 1, 10.75']);
      echo $form->field($model, 'success_rate')->textInput(['maxlength' => true,
        'placeholder' => 'Success rate (gives an estimate of risk involved) as a number between 0 and 1']);
      // echo $form->field($model, 'responsible_partner')->textInput(['maxlength' => true]);
    } ?>

    <?= $form->field($model, 'comments')->textarea(['rows' => 6]); ?>

    <!-- // the following is for the optimisticLock (locking the records) -->
    <?php echo Html::activeHiddenInput($model, 'version'); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update',
          ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a('Cancel', $model->isNewRecord ? ['index'] : ['index', 'id' => $model->id], ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
