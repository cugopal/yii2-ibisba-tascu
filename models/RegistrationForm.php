<?php

namespace app\models;

use dektrium\user\models\Profile;
use dektrium\user\models\RegistrationForm as BaseRegistrationForm;
use dektrium\user\models\User;
use Yii;

class RegistrationForm extends BaseRegistrationForm
{
    /**
     * Add a new field
     * @var string
     */
    public $name;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = parent::rules();
        $rules[] = ['name', 'required'];
        $rules[] = ['name', 'string', 'max' => 255];
        $rules[] = ['email', 'required'];
        $rules[] = ['email', 'email', 'message'=>'Invalid email address'];
        return $rules;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        $labels = parent::attributeLabels();
        $labels['name'] = \Yii::t('user', 'Name');
        return $labels;
    }

    /**
     * @inheritdoc
     */
    public function loadAttributes(User $user)
    {
        // here is the magic happens
        $user->setAttributes([
            'email'    => $this->email,
            'username' => $this->username,
            'password' => $this->password,
        ]);
        /** @var Profile $profile */
        $profile = \Yii::createObject(Profile::className());
        $profile->setAttributes([
            'name' => $this->name,
        ]);
        $user->setProfile($profile);
    }

    /**
 * Registers a new user account. If registration was successful it will set flash message.
 *
 * @return bool
 */
  public function register()
  {
      if (!$this->validate()) {
          return false;
      }

      /** @var User $user */
      $user = Yii::createObject(User::className());
      $user->setScenario('register');
      $this->loadAttributes($user);

      if (!$user->register()) {
          return false;
      }

      Yii::$app->session->setFlash(
          'info',
          Yii::t(
              'user',
              'Your account has been created and will be activated once the admin aprroves it. If you need further assistance please write to '.Yii::$app->params['adminEmail']
          )
      );

      return true;
  }

}
