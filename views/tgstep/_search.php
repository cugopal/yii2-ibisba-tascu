<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MouldsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tgstep-search">

  <?php $form = ActiveForm::begin([
    'layout' => 'inline',
    'action' => ['index'],
    'method' => 'get',
  ]); ?>

  <!-- <div class="form-group"> -->
  <div class="input-group">
    <?= $form->field($model, 'search_keyword')->textInput([
      'placeholder'=>'Type a keyword to search DBTL steps table'])
      ->label(false) ?>
    <span class="input-group-btn">
      <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
    </span>
  </div>

  <?= Html::a('Reset search filters', ['index'], ['class' => 'btn btn-default']) ?>
    <!-- </div> -->

  <?php ActiveForm::end(); ?>

</div>
