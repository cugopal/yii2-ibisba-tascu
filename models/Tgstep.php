<?php
namespace app\models;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use \yii2tech\ar\softdelete\SoftDeleteBehavior;

/**
 * @property string $description
 * @property integer $parent_id
 */
class Tgstep extends ActiveRecord
{
  //public $new_parent_id;
  //public $sibling_id;
  public $before_or_after;
  public $move_to_target;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dbtl_steps';
    }

    public function attributeLabels()
    {
      return [
        'step_desc' => 'Description',
      ];
    }

    public function behaviors()
    {
      return [
        'softDeleteBehavior' => [
          'class' => SoftDeleteBehavior::className(),
          'softDeleteAttributeValues' => [
            'is_deleted' => true
          ],
        ],
        'history' => [
          'class' => \nhkey\arh\ActiveRecordHistoryBehavior::className(),
          'manager' => '\nhkey\arh\managers\DBManager',
          'ignoreFields' => ['is_deleted', 'version', 'created', 'last_modified'],
        ],
      ];
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['before_or_after'], 'string'],
            [['before_or_after'], 'trim'],
            [['before_or_after'], 'in',
              'range'=>['before', 'after', 'in'],
              'message'=>'Choose one of before, after, or in'],
            [['move_to_target'], 'integer'],
            [['step_name'], 'required'],
            [['step_name'], 'string', 'max'=>250],
            [['step_name'], 'trim'],
            [['step_desc'], 'safe'],
            [['step_desc'], 'string', 'max'=>500],
            [['step_desc'], 'trim'],
            [['responsible_partner'], 'safe'],
            [['responsible_partner'], 'string', 'max'=>50],
            [['responsible_partner'], 'trim'],
            [['organism'], 'safe'],
            [['organism'], 'string', 'max'=>50],
            [['organism'], 'trim'],
            [['part_of_service'], 'safe'],
            [['part_of_service'], 'string', 'max'=>50],
            [['part_of_service'], 'trim'],
            [['duration_in_days'], 'safe'],
            [['duration_in_days'], 'number'],
            [['success_rate'], 'safe'],
            [['success_rate'], 'number', 'min'=>0, 'max'=>1],
            [['sop_url'], 'safe'],
            [['sop_url'], 'string', 'max'=>1000],
            [['sop_url'], 'trim'],
            [['comments'], 'safe'],
            [['comments'], 'string', 'max'=>1000],
            [['comments'], 'trim'],
            [['parent_id'], 'integer'],
            [['version'], 'integer'],
            [['version'], 'default', 'value'=>0],
            [['created', 'last_modified'], 'safe'],
        ];
    }

    public function optimisticLock() {
      return 'version';
    }

    public function countChildren($id)
    {
      /* the number of elements with id as the parent_id
       * gives the number of children of parent_id */
      return self::find()->where(['parent_id'=>$id])->count();
    }

    public function getChildren($id)
    {
      /* the ids of elements with parent_id = $id */
      return ArrayHelper::map(
          self::find()->select('id')->where([
            'is_deleted'=>false,'parent_id'=>$id])->orderBy([
            'position' => SORT_ASC])->all(),
            'id', 'id');
    }

    /*
    public function getChildrenNames($id)
    {
      return ArrayHelper::map(
          self::find()->select('id','step_name')->where([
            'is_deleted'=>false,'parent_id'=>$id])->all(),
            'id', 'step_name');
    } */
    /* Get the ids of this element and all other elements under this branch */
    public function getBranch($id) {
      $lineage_ids = [(int)$id=>(int)$id];
      do {
        // it is really important to create an associative array (map)
        // due to the PHP's exceptional behavior of array_merge in the next step
        $children_ids = ArrayHelper::map(
          self::find()->select('id')->where(['is_deleted'=>false,'parent_id'=>$id])->all(),
          'id', 'id');
        $lineage_ids = $lineage_ids + $children_ids;
        $id = $children_ids;
      } while($id);
      return $lineage_ids;
    }
}
?>
