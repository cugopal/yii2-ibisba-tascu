<?php

namespace app\components;

use dektrium\user\models\User;
use dektrium\user\models\Token;
use Yii;
use yii\base\Component;


class MyMailer extends \dektrium\user\Mailer
{
    public function sendWelcomeMessage(User $user, Token $token = null, $showPassword = false)
    {
        return $this->sendMessage(
            Yii::$app->params['adminEmail'],
            \Yii::$app->name.': confirm user: '.$user->username,
            'confirmation',
            ['user' => $user, 'token' => $token, 'module' => $this->module, 'showPassword' => $showPassword]
        );
    }

    public function sendConfirmationMessage(User $user, Token $token = null, $showPassword = false)
    {
        return $this->sendMessage(
            $user->email,
            \Yii::$app->name.': user account '.$user->username.' confirmed!',
            'welcome',
            ['user' => $user, 'token' => $token, 'module' => $this->module, 'showPassword' => $showPassword]
        );
    }
}
