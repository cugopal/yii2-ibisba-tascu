

<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
* @var yii\web\View              $this
* @var yii\widgets\ActiveForm    $form
* @var dektrium\user\models\User $user
*/

$this->title = Yii::t('user', 'Sign up');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">

      <!--  <p><a class="btn btn-lg btn-success" href="http://www.yiiframework.com">Get started with Yii</a></p> -->
  <!--  </div> -->
    <div class="col-md-4 col-md-offset-4">
        <div class="panel panel-default">
            <div class="panel-heading">
            <h3 class="panel-title"><?= Html::encode($this->title) ?></h3>
            </div>

            <div class="panel-body">
                <?php $form = ActiveForm::begin([
                    'id' => 'registration-form',
                ]); ?>

                <?= $form->field($model, 'name')->textInput(['placeholder'=>'First Last']) ?>


                <!-- the 'onchange' code copies the value of this field to
                     the username text field -->
                <?= $form->field($model, 'email')->textInput([
                  'placeholder'=>'Valid email address to confirm registration',
                  'onchange'=>'$( "#'.Html::getInputId($model, 'username').'").val($(this).val());']) ?>

                <!-- the user can't modify the username field. Instead the email
                    field gets copied to the username -->
                <?= $form->field($model, 'username')->textInput([
                  'placeholder' => 'Same as the email',
                  'readonly'=> 'true']) ?>

                <?= $form->field($model, 'password')->passwordInput() ?>

                <?= Html::submitButton(Yii::t('user', 'Sign up'), ['class' => 'btn btn-success btn-block']) ?>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
        <p class="text-center">
            <?= Html::a(Yii::t('user', 'Already registered? Sign in!'), ['/user/security/login']) ?>
        </p>
    </div>
</div>
