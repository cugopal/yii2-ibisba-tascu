<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

$this->title = 'TasCu: edit history of DBTL step '.$model_id;
$this->params['breadcrumbs'][] = ['label' => 'Index', 'url' => 'index'];
$this->params['breadcrumbs'][] = ['label' => $model_id, 'url' => ['view', 'id' => $model_id]];
$this->params['breadcrumbs'][] = 'History';
?>
<div class="record-history">

<?php Pjax::begin(); ?>
<?= GridView::widget([
        'dataProvider' => $dataProvider,
        'tableOptions' => ['class' => 'table-striped table-condensed table-hover table-bordered'],
        'headerRowOptions' => ['class' => 'breadcrumb'],
        'columns' => [
            //'table',
            [
              'attribute' => 'field_id',
              'format' => 'raw',
              'value' => function($data) {
                return Html::a($data->field_id, ['tgstep/view', 'id' => $data->field_id], ['data-pjax' => 0]);
              }
            ],
            //'field_id',
            'field_name',
            'old_value',
            'new_value',
            'date',
            //'type',
            'user_id'
        ],
    ]); ?>
  </div>
<?php Pjax::end(); ?>
